﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Store.IndexViewModels;

using Store.Models;

namespace Store.Controllers
{
    public class HomeController : Controller
    {
        ProductContext context;

        public HomeController(ProductContext context)
        {
            this.context = context;
          
        }

        public async Task<IActionResult> Index(int page =1)
        {
            IndexViewModel model = new IndexViewModel();
            IQueryable<Product> products = context.Products.Include(p => p.Company).OrderByDescending(p => p.Date);
            model.Products  = context.Products.Include(p => p.Company).OrderByDescending(p => p.Date).ToList();
            model.Companies = context.Company.ToList();
            model.Products = context.Products.Include(p => p.Category).OrderByDescending(p => p.Date).ToList();
            model.Categories = context.Categories.ToList();

            int pageSize = 3;
            int count = products.Count();
            products = products.Skip((page - 1) * pageSize).Take(pageSize);

            model.PageViewModel = new PageViewModel(count, page, pageSize);
            model.Products = await products.AsNoTracking().ToListAsync();
            
            return View(model);
        }


        public IActionResult Create()
        {
            List<Company> companies = context.Company.ToList();
            ViewBag.Companies = new SelectList(companies, "Id", "Name");
            List<Category> categories = context.Categories.ToList();
            ViewBag.Categories = new SelectList(categories, "Id", "Name");

            return View();
        }

        [Authorize]
        [HttpPost]
        public IActionResult Create(Product product)
        {
            context.Products.Add(product);
            context.SaveChanges();

            return RedirectToAction("Index");
        }

        public IActionResult Details(int id)
        {
            Product phone = context.Products.Include(p => p.Company).Include(p => p.Category).FirstOrDefault(p => p.Id == id);
            return View(phone);
        }



       
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public bool CheckName(string name)
        {
            List<Product> products = context.Products.Where(c => c.Name == name).ToList();
            if (products.Any())
            {
                return false;
            }
            else
            {
                return true;
            }

        }
        [HttpPost]
        public IActionResult SetLanguage(string culture, string returnUrl)
        {
            Response.Cookies.Append(
                CookieRequestCultureProvider.DefaultCookieName,
                CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(culture)),
                new CookieOptions { Expires = DateTimeOffset.UtcNow.AddYears(1) }
            );

            return LocalRedirect(returnUrl);
        }
    }
}
