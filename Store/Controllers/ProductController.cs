﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Store.IndexViewModels;
using Store.Models;

namespace Store.Controllers
{
    public class ProductController : Controller
    {
        private ProductContext context;


        public ProductController(ProductContext context)
        {
            this.context = context;

        }
        public async Task<IActionResult> ChooseCategory(string categoryName, int page = 1)
        {
            Category category = context.Categories.FirstOrDefault(p => p.Name == categoryName);

            //List<Product> products = context.Products.Where(P => P.CategoryId == category.Id ).Include(p => p.Company).Include(p => p.Category).OrderByDescending(p => p.Date).ToList();
            IndexViewModel indexView = new IndexViewModel();

            IQueryable<Product> products = context.Products.Where(P => P.CategoryId == category.Id).Include(p => p.Company).Include(p => p.Category).OrderByDescending(p => p.Date);

            indexView.Products = context.Products.Include(p => p.Company).OrderByDescending(p => p.Date).ToList();
            indexView.Companies = context.Company.ToList();
            indexView.Products = context.Products.Include(p => p.Category).OrderByDescending(p => p.Date).ToList();
            indexView.Categories = context.Categories.ToList();

            int pageSize = 2;
            int count = products.Count();
            products = products.Skip((page - 1) * pageSize).Take(pageSize);

            indexView.PageViewModel = new PageViewModel(count, page, pageSize, categoryName);
            indexView.Products = await products.AsNoTracking().ToListAsync();

            return View(indexView);
        }
        public IActionResult Details(int id)
        {
            Product phone = context.Products.Include(p => p.Company).Include(p => p.Category).FirstOrDefault(p => p.Id == id);
            return View(phone);
        }



        public IActionResult Search(string categoryName,string query,  int priceFrom,  int priceTo = Int32.MaxValue,  int page = 1 )
        {
            Category category = context.Categories.FirstOrDefault(p => p.Name == categoryName);
            List<Product> products = context.Products.Where(p => p.Category == category).ToList();
            IndexViewModel model = new IndexViewModel();


            int pageSize = 2;
            int count = products.Count();
            products = products.Where
            (
                p => p.Name.ToLower().Contains(String.IsNullOrEmpty(query) ? p.Name.ToLower() : query.ToLower())

            ).ToList();

            products = products.Where(
                p =>
                    p.Price >= priceFrom && p.Price <= priceTo
            ).ToList();

          
            model.Products = products;
            model.Companies = context.Company.ToList();
            model.Categories = context.Categories.ToList();
            model.PageViewModel = new PageViewModel(count, page, pageSize, categoryName);
            return View("ChooseCategory", model);

        }

        [Authorize]
        public IActionResult Edit(int id)
        {
            List<Company> companies = context.Company.ToList();
            ViewBag.Companies = companies;
            List<Category> categories = context.Categories.ToList();
            ViewBag.Categories = categories;
            Product product = context.Products.FirstOrDefault(p => p.Id == id);
            return View(product);
        }

        [Authorize]
        [HttpPost]
        public IActionResult Edit(Product product)
        {
            //Product phoneToUpdate = context.Products.First(p => p.Id == product.Id);
            //product = phoneToUpdate;
            context.Products.Update(product);
            context.SaveChanges();


            return RedirectToAction("Details", "Product", new { product.Id });
        }

        public IActionResult SearchAjax()
        {
            return View();
        }

        public IActionResult SearchAjaxResult(string keyWord)
        {
            List<Product> products = context.Products.ToList();
           
                products = context.Products.Where(u =>
                u.Name.Contains(keyWord)).ToList();         
                                       
            return PartialView("ProductSearchResult", products);
        }
      




    }
}