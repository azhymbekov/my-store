﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Store.Models
{
    public class Company
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Введите Компанию")]
        public string Name { get; set; }
    }
}
