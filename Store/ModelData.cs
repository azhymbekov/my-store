﻿using NetShop.Models;
using Store.Models;
using System;
using System.Linq;

namespace Store
{
    public class ModelData
    {
        public static void Initialize(ProductContext context)
        {

            if (!context.Roles.Any())
            {
                context.Roles.AddRange
                    (
                        new Role() { Name = "admin" },
                        new Role() { Name = "user" }
                    );
            }

            if (!context.Users.Any())
            {
                context.Users.AddRange
                    (
                        new User()
                        {
                            RoleId = 1,
                            Name = "admin",
                            Email = "admin@gmail.com",
                            Password = "admin"
                        }

                    ); context.SaveChanges();
            }


            if (!context.Company.Any())
            {
                context.Company.AddRange
                    (
                        new Company() { Name = "Samsung"},
                        new Company() { Name = "Xiaomi"},
                        new Company() { Name = "Apple" }
                    );
            }
            context.SaveChanges();

            if (!context.Categories.Any())
            {
                context.Categories.AddRange
                    (
                        new Category() { Name = "Телефон"},
                        new Category() { Name = "Телевизор" },
                        new Category() { Name = "Ноутбук" },
                        new Category() { Name = "Разное"}
                    );
            }
            context.SaveChanges();
            if (!context.Products.Any())
            {
                context.Products.AddRange
                    (
                        new Product()
                        {

                            CompanyId = context.Company.FirstOrDefault(c => c.Name == "Samsung").Id,
                            CategoryId = context.Categories.FirstOrDefault(c => c.Name == "Телевизор").Id,
                            Name = "S8",
                            Price = 900,                           
                            Date = DateTime.Parse("12.12.2017"),
                            

                        },
                        new Product()
                        {
                         
                            Name = "Iphone X",
                            CompanyId = context.Company.FirstOrDefault(c => c.Name == "Apple").Id,
                            CategoryId = context.Categories.FirstOrDefault(c => c.Name == "Телефон").Id,
                            Price = 1000,
                            Date = DateTime.Parse("12.12.2017"),
                        
                        },
                        new Product()
                        {
                            
                            Name = "Mi7",
                            CompanyId = context.Company.FirstOrDefault(c => c.Name == "Xiaomi").Id,
                            CategoryId = context.Categories.FirstOrDefault(c => c.Name == "Разное").Id,
                            Price = 500,
                            Date = DateTime.Parse("12.12.2017"),
                           
                        }
                    );
                context.SaveChanges();
            }
        }
    }
}
