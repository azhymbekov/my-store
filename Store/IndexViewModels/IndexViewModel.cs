﻿
using NetShop.Models;
using Store.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Store.IndexViewModels
{
    public class IndexViewModel
    {

        public List<Company> Companies { get; set; }
        public List<Product> Products { get; set; }
        public List<Category> Categories { get; set; }
       



        public PageViewModel PageViewModel { get; set; }
    }
}
