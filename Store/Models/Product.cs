﻿using Microsoft.AspNetCore.Mvc;

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Store.Models
{
    public class Product
    {
        [Key]
        public int Id { get; set; }
        [Required(ErrorMessage = "Введите имя товара")]
        [Remote(action:"CheckName",controller: "Home", ErrorMessage ="Такой товар уже имеется")]
        public string Name { get; set; }

        [Required]
        [Range(1, 1000000, ErrorMessage = "Недопустимое значение")]

        public int Price { get; set; }

        [StringLength(100, ErrorMessage = "Описание не должно содержать более 100 символов")]
        public string Description { get; set; }

        
        [Required( ErrorMessage = "Недопустимое значение даты поставки")]
        
        public DateTime Date { get; set; }

        public int CompanyId { get; set; }
        public Company Company { get; set; }

        public int CategoryId { get; set; }
        public Category Category { get; set; }

        


}
}
